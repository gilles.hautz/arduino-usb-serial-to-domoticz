#!/bin/bash

while read -r line < /dev/ttyUSB0; do
	newline=$(echo $line | sed -e s'/\r//')
	echo "newline = $newline"
	if [ $newline = "RELAY:1" ]; then
		request="https://192.168.1.55/json.htm?type=command&param=udevice&idx=21&nvalue=1"
	fi
	if [ $newline = "RELAY:0" ]; then
		request="https://192.168.1.55/json.htm?type=command&param=udevice&idx=21&nvalue=0"
	fi
	echo "URL = $request"
	curl -k $request
done
