/*
  The circuit:
  - LED attached from pin 2 to ground
  - pushbutton attached from pin 3 to +5V
  - 10 kilohm resistor attached from pin 3 to ground
  - relay connected to pin 4
*/

const String osversion = "1.1.3";

char chaine;

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 2;    // the number of the pushbutton pin
const int ledPin = 12;      // the number of the LED pin
const int relayPin = 4;

// Variables will change:
int ledState = HIGH;         // the current state of the output pin
int relayState = HIGH;	     // When HIGH relay is OFF
int buttonState;             // the current reading from the input pin
int lastButtonState = LOW;   // the previous reading from the input pin
//int lastPirState = LOW;

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number that can't be stored in an int.
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers


void setup() {
  Serial.begin(9600);
  Serial.print("VERSION=");
  Serial.println(osversion);
  pinMode(buttonPin, INPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(relayPin, OUTPUT);

  // set initial LED state
  Serial.print("LEDSTATE=");
  Serial.println(ledState);
  digitalWrite(ledPin, ledState);
  Serial.print("RELAY:");
  Serial.println(relayState);
  digitalWrite(relayPin, relayState);

}

void loop() {
	if ( Serial.available()) {
		char chaine = Serial.read();
		if(chaine == '1') {
			Serial.println("RELAY:1") ;
	      		relayState = LOW;
			ledState = LOW;
			digitalWrite(relayPin, relayState);
			digitalWrite(ledPin, ledState);
		}
		if(chaine == '0') {
			Serial.println("RELAY:0") ;
	      		relayState = HIGH;
			ledState = HIGH;
			digitalWrite(relayPin, relayState);
			digitalWrite(ledPin, ledState);
		}
	}
	

  int reading = digitalRead(buttonPin);

  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != buttonState) {
      buttonState = reading;
      // only toggle the LED if the new button state is HIGH
      if (buttonState == HIGH) {
	if (relayState == HIGH) {
	        Serial.println("RELAY:1");
	} else {
	        Serial.println("RELAY:0");
	}
        ledState = !ledState;
        relayState = !relayState;
      }
    }
  }

  // set the LED:
  digitalWrite(ledPin, ledState);
  digitalWrite(relayPin, relayState);

  // save the reading. Next time through the loop, it'll be the lastButtonState:
  lastButtonState = reading;
}
